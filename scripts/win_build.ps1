Write-Output "Building idp3-ide..."
Set-Location "idp-ide"
stack build
stack install --local-bin-path .
Set-Location ..

Write-Output "Building Electron wrapper..."
Set-Location "electron-wrapper"
npx electron-packager . --platform=win32 --arch=x64 --icon="../supplementary/idpico.ico"
Set-Location ..

Write-Output "Packing files..."
# Move electron
Move-Item "./electron-wrapper/idp3-ide-win32-x64" "./idp3-ide-win32-x64"
# Move webID
New-Item -ItemType Directory -Path "./idp3-ide-win32-x64/resources/app/webidp/" -Force
Copy-Item "./idp-ide/webID.exe" "./idp3-ide-win32-x64/resources/app/webidp/"
Copy-Item -Recurse "./idp-ide/idp" "./idp3-ide-win32-x64/resources/app/webidp/"
# Download idp3
Invoke-WebRequest -Uri "https://dtai.cs.kuleuven.be/static/krr/files/releases/idp/idp3-3.7.1-Win.zip" -OutFile "idp3-3.7.1-Win.zip"
Expand-Archive -Path "./idp3-3.7.1-Win.zip" -DestinationPath "./idp3-ide-win32-x64/resources/app/"
Remove-Item "./idp3-3.7.1-Win.zip" -Force -Recurse
# If old build exists remove it
$directoryPath = "./build/idp3-ide-win32-x64"
if (Test-Path $directoryPath -PathType Container) {
    Remove-Item $directoryPath -Force -Recurse
}
# Move new build to the build directory 
Move-Item -Path "./idp3-ide-win32-x64" -Destination "./build/" -Force


