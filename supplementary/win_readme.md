# IDP3-IDE installation instructions (Windows)

After downloading and extracting the IDP3-IDE package, you should be reading this document. Follow these instructions to make `IDP3-IDE` available on your machine.

1) Move the `idp3-ide-win32-x64` directory to the `C:\Program Files (x86)\` directory (or if (x86) does not exist to `c:\Program Files\`).
2) Enter the `C:\Program Files (x86)\idp3-ide-win32-x64\` right-click on `idp3-ide.exe` and select the option to add it to the start/taskbar.

*Note:* It is possible that when running for the first time you see the message whether to make service globally available, you can decline this.