		///HERE
(function() {var templates = 
	{  
   "name":"idp",
   "context":"idp",
   "templates":[  
      {  
         "name" : "theory",
         "description":"declare a theory block",
         "template" : "theory ${T} : ${V} {\n\t ${cursor}\n}"
      },
      {  
         "name" : "vocabulary",
         "description":"declare a vocabulary block",
         "template" : "vocabulary ${V} {\n\t ${cursor}\n}"
      },
      {
         "name" : "LTCvocabulary",
         "description":"declare an LTC vocabulary block",
         "template" : "LTCvocabulary ${V}(Time,Start,Next) {\n\ttype Time isa nat\n\tStart : Time\n\tNext(Time):Time\n\t ${cursor}\n}"
      },
      {
      	"name" : "structure",
      	"description":"declare a structure block",
        "template" : "structure ${S} : ${V} {\n\t ${cursor}\n}"
      },
      {
      	"name" : "procedure",
      	"description":"declare a procedure",
        "template" : "procedure ${main}() {\n\tstdoptions.nbmodels=1\n\tprintmodels(modelexpand(T,S))\n\t ${cursor}\n}"
      },
      {
      	"name" : "reachability",
      	"description":"express the transitive closure of a relation",
        "template" : "{\n\t ${Reach}(x,y)<- ${Edge}(x,y).\n\t${Reach}(x,y) <-?z : ${Edge}(x,z) & ${Reach}(z,y).\n}"
      },
      {
      	"name" : "type",
      	"description":"declare a new type",
        "template" : "type ${T}"
      },
      {
        "name" : "min",
        "description" : "compute the minimum of a predicate",
        "template" : "min{x : ${P}(x) : x}"
      },
      {
        "name" : "max",
        "description" : "compute the maximum of a predicate",
        "template" : "max{x : ${P}(x) : x}"
      },
      {
        "name" : "product",
        "description" : "compute the product of a predicate",
        "template" : "prod{x : ${P}(x) : x}"
      },      
      {
        "name" : "sum",
        "description" : "compute the sum of a predicate",
        "template" : "sum{x : ${P}(x) : x}"
      },
      {
        "name" : "cardinality",
        "description" : "compute the cardinality of a predicate",
        "template" : "#{x : ${P}(x)s}"
      },
      {
        "name" : "constructed type",
        "description" : "declaration of a constructed type",
        "template" : "constructed from {${Constructor}}"
      },

   ]
};
	CodeMirror.templatesHint.addTemplates(templates);})();
