echo "Building for Linux..."

# Setting up: Update, Stack, Zip, Electron
echo "Updating..."
apt-get update
echo "Installing stck"
curl -sSL https://get.haskellstack.org/ | sh
echo "Installing zip"
apt install zip
echo "Installing electron"
npm install electron@latest
npm install --save-dev @electron/packager

# Building webID
echo "Building idp3-ide..."
cd idp-ide
stack build
stack install --local-bin-path .
cd ..

# Build electron wrapper
echo "Building Electron wrapper..."
cd electron-wrapper
npx electron-packager . --platform=linux --arch=x64 --icon=./../supplementary/idpico.ico
cd ..

# Pack files
echo "Packing files..."
mv ./electron-packager/idp3-ide-linux-x64 ./idp3-ide-linux-x64
mkdir ./idp3-ide-linux-x64/webidp/
cp ./idp-ide/webID ./idp3-ide-linux-x64/resources/app/webidp/
cp -r ./idp-ide/idp ./idp3-ide-linux-x64/resources/app/webidp/
cp ./supplementary/idp3.desktop ./idp3.desktop
cp ./supplementary/idpico.png ./idp3-ide-linux-x64/
cp ./idp3-ide-linux-x64/ ./build/
