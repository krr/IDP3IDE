## Release v0.3.4

### Fixed

- [Fixed linte issue](https://gitlab.com/krr/IDP3IDE/-/commit/79ba79b40fea87929b117313e3635addfd080a6e)

## Release v0.3.3

### Fixed

- [Fixed links in the help menu](https://gitlab.com/krr/IDP3IDE/-/commit/40e576c08eb6a9a6cb443616a8071a019e610d8d)

### Added 

- [Version display added to the IDE](https://gitlab.com/krr/IDP3IDE/-/commit/395664df1d8f16332cb677ffb1fc95798c579323)

## Release v0.3.2

### Fixed (1 changes)

- [Fixed problem with idp3 calls on dfferent platforms](https://gitlab.com/krr/IDP3IDE/-/commit/b13afffcd72783d1e05b19065f56717d4e51d673)

## Release v0.3.1

### Fixed (2 changes)

- [IDP3 command parameters](https://gitlab.com/krr/IDP3IDE/-/commit/c2839035ce265ab18d11fecbe70bafe47ac53178)
- [IDP3 binaries error message](https://gitlab.com/krr/IDP3IDE/-/commit/4b95a2d96a51a433cd11fcc6b8c7db14c4f003d8)

## Release v0.3.0

### added (1 change)

- [Added installation instructions with binaries](https://gitlab.com/krr/IDP3IDE/-/commit/0ecc7265301ff71e5d926df4760ef92a816a5ed4) - Now releases are including instalation instructions. 


## Release v0.2.6

### added (1 change)

- [Add IDP3 binaries](https://gitlab.com/krr/IDP3IDE/-/commit/2e4c9cfffd254630a26def92f0c378b3ba7621f2) - Now releases are including idp3 binaries. 


## Release v0.1.0

### added (1 change)

- [Initial release](https://gitlab.com/krr/IDP3IDE/-/commit/1c4f254eb02569c8f696b607a9796a2647e99394)

