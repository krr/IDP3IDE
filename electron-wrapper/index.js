const { app, BrowserWindow } = require('electron');
const exec = require('child_process').spawn;
const fs = require('fs');
var child;

function execute(command, args, directory, callback) {
    child = exec(command, args, { cwd: directory }, (error, stdout, stderr) => { 
        callback(stdout);
    });
};

function createWindow () {
	//Creante window conf
	const win = new BrowserWindow({
		show: false,
		autoHideMenuBar: true,
		webPreferences: {
			contextIsolation: true
		}
	});
	
	// Get the working directory of the app
	var appCwd = app.getAppPath();

	// Start the server 
	// In case of MacOs and Linux
	if (process.platform !== 'win32') {
		const filePath = appCwd+'/idp3/bin/idp';
		fs.access(filePath, fs.constants.F_OK, (err) => {
  			if (err) {
   		 		console.log('No idp.bat file found in this package, using default (idp).');
				execute('./webID', ['idp'], appCwd+'/webidp/',  (output) => {});
  			} else {
				console.log('idp.bat file found in this package.');
				execute('./webID', [appCwd+'/idp3/bin/idp'], appCwd+'/webidp/',  (output) => {});
  			}
		});		
	// In case of Windows
	} else if (process.platform == 'win32') {
		const filePath = appCwd+'\\idp3\\bin\\idp.bat';
		fs.access(filePath, fs.constants.F_OK, (err) => {
  			if (err) {
   		 		console.log('No idp.bat file found in this package, using default (idp).');
				execute('webID.exe', ['idp'], appCwd+'\\webidp\\',  (output) => {});
  			} else {
				console.log('idp.bat file found in this package.');
				execute('webID.exe', [appCwd+'\\idp3\\bin\\idp'], appCwd+'\\webidp\\',  (output) => {});
  			}
		});		
	}

	win.loadURL('http://localhost:4004/idp/');
	win.maximize();
	win.show();
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
	child.kill();
    app.quit();
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
})
