# Features
- [x] (NR) Add a clear button for the console
- [x] (NR) IDP binary configurator (first try if IDP is on the path and if not ask the user to provide a path).
- [ ] (OP) Structure copy 
- [ ] (OP) Structure compare
- [x] (NR) Welcome dialogue and tutorial
- [x] (NR) Workspace selector with file browse option
- [x] (NR) Add font size setup 
- [x] (NR) Restore settings to default
- [x] (NR) Show/Hide hidden directories
- [ ] (OP) Save workspace settings
 
# Improvements 
- [x] (NR) Update glyphicons and use terminal for output
- [x] (NR) Add checks and error reports on the backend (Haskell)
- [ ] (OP) Better syntax highlighting? 
- [x] (NR) IDP icon for the dark theme.
- [x] (NR) Custom scrollbars

# Development
- [ ] (OP) Add comments