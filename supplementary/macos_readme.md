# IDP3-IDE installation instructions (MacOS)

After downloading and extracting the IDP3-IDE package, you should be reading this document. Follow these instructions to make `IDP3-IDE` available on your machine.

1) Open the `idp3-ide-darwin-x64` extracted directory.
2) Move the `ide3-ide` package to the `/Applications/` directory.
3) Go to `/Applications/` and right-click on the `idp3-ide` app and press open.
4) A warning window will appear that the app has been downloaded from the internet... Click open.
