#!/bin/bash

if [ $# -eq 0 ]; then

	#IDP already installed
	if [ -d "/Applications/idp3-desktop-app.app" ]; then
		read -p "It looks like IDP3 is already installed! Would you lik to delete it (ATTENTION: this will remove work directory as well, please backup your code first!!!)? [Y/n]?:" del

		if [ "$del" = "Y" ] || [ "$del" = "y" ]; then
			echo "Removing IDP3 files..."
			sudo rm -r /Applications/idp3-desktop-app.app
			echo "Removing is finished! Please note that this procedure didn't remove the symlink created in the installation procedure!"
		fi
		exit
	fi

	#unzip is not installed
	if ! command -v unzip &> /dev/null
	then
		echo "unzip command could not be found, please install it and try the script again."
		exit
	fi

	#wget is not installed
	if ! command -v wget &> /dev/null
	then
		echo "wget command could not be found, please install it and try the script again."
		exit
	fi

	# Getting files
	echo "Downloading files..."
	wget -q https://bitbucket.org/krr/idp3-desktop/downloads/idp3-desktop-app-darwin-x64.zip

	# Extracting files
	echo "Extracting idp3-desktop binaries..."
	unzip -q idp3-desktop-app-darwin-x64.zip

	# Moving files 
	echo "Move files..."
	sudo mv idp3-desktop-app-darwin-x64/idp3-desktop-app.app /Applications/idp3-desktop-app.app

	# Removing security restictions
	echo "Removing security checks..."
	sudo xattr -d com.apple.quarantine /Applications/idp3-desktop-app.app

	# Create a workdir symlink
	read -p "Please enter the path for linking the work directory (Press [Enter] for the default: ~/Documents/idp3-workfolder/):" path
	if [ "$path" ]; then
		echo "Create a link to: " $path
		ln -s /Applications/idp3-desktop-app.app/Contents/Resources/webidp/workfolder/ ${path/#~\//$HOME\/}
	else
		echo "Create a link to ~/Documents/idp3-workfolder"
		ln -s /Applications/idp3-desktop-app.app/Contents/Resources/webidp/workfolder/ ~/Documents/idp3-workfolder
	fi

	# Clean directory
	echo "Removing temp files..."
	rm idp3-desktop-app-darwin-x64.zip

	echo "Instalation finished!"
else
	echo "This script does not accept any arguments!!!"
fi
