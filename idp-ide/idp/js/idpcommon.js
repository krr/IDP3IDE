String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

$.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  }
});

$.fn.setCursorPosition = function(pos) {
  this.each(function(index, elem) {
    if (elem.setSelectionRange) {
      elem.setSelectionRange(pos, pos);
    } else if (elem.createTextRange) {
      var range = elem.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  });
  return this;
};

// Get the code from the editor
function getCode(){
    return window.editor.mirror.getValue();
}

// Run the code
function run(){
  myLayout.open('east')

  // Get the file name
  if($("#filename").html()){
    var filename = $("#filename").html();
    consoleReportMessage("Success: Starting execution of /" + filename + "!", "success");

    // Create object
    var obj =
    {"code" : getCode(),
      "inference" : 2,
      "filename" : filename,
      "eInput" : ""
    };

    // Create the socket
    makeSocket(obj);
  } else {
    // If no file selected printe an error
    consoleReportMessage("Error: Please select a file first!");
  }
}

function hideTooltip(tt) {
    if (!tt.parentNode) return;
    if (tt.style.opacity == null) rm(tt);
    tt.style.opacity = 0;
    setTimeout(600);
}

function showTooltip(e, content) {
    var tt = document.createElement("div");
    var inner = document.createElement("div");
    inner.className = "CodeMirror-lint-message-core";
    tt.className = "CodeMirror-lint-tooltip";
    inner.appendChild(document.createTextNode(content));
    tt.appendChild(inner);
    document.body.appendChild(tt);

    function position(e) {
      if (!tt.parentNode) return CodeMirror.off(document, "mousemove", position);
      tt.style.top = Math.max(0, e.clientY - tt.offsetHeight - 5) + "px";
      tt.style.left = (e.clientX + 5) + "px";
    }
    CodeMirror.on(document, "mousemove", position);
    position(e);
    if (tt.style.opacity != null) tt.style.opacity = 1;
    return tt;
}

function showTooltipFor(e, content, node) {
  var tooltip = showTooltip(e, content);
  function hide() {
    CodeMirror.off(node, "mouseout", hide);
    hideTooltip(tooltip); tooltip = null;
  }
  var poll = setInterval(function() {
    if (tooltip) for (var n = node;; n = n.parentNode) {
      if (n == document.body) return;
      if (!n) { hide(); break; }
    }
    if (!tooltip) return clearInterval(poll);
  }, 400);
  CodeMirror.on(node, "mouseout", hide);
}

function makemarker(labels){
      var marker = document.createElement("div");
      marker.className = "CodeMirror-lint-marker-core";
      marker.innerHTML = " ";
      CodeMirror.on(marker, "mouseover", function(e) {
      showTooltipFor(e, labels, marker);
      });
      return marker;
}

function showCore(arg){
  if(!arg) {
    return;
  }

  arg.forEach(function(coreLine){
    var lineNr = coreLine.line_nr - 1;
    var charNr = 0;
    var firstToken = window.editor.mirror.getTokenAt(CodeMirror.Pos(lineNr,1));
    if(firstToken.type == "whitespace"){
      charNr = firstToken.end;
    }
    var ann = {};
    ann.from = CodeMirror.Pos(lineNr, charNr);
    ann.to = CodeMirror.Pos(lineNr+1, 0);
    ann.message = coreLine.hasmessage;
    ann.severity = "core";
    coreMarkers.push(
      window.editor.mirror.markText(
      ann.from,
      ann.to,
      {className:"CodeMirror-lint-mark-core",
       __annotation: ann,
      })
    );
    window.editor.mirror.setGutterMarker(lineNr, "core-gutter", makemarker(coreLine.hasmessage));

  });
}

function deriveRoute(){
  route = window.location.pathname.split('/');
  routeStr = ""
  for(var i = 0; i < route.length-1;i++){
    routeStr += route[i] + "/";
  }
  return routeStr
}

function tryToParseVisualization(str) {
  try{
    return JSON.parse(str);
  }catch(err){
    return undefined;
  }
}

function tryToParseStructure(str){
  // Split string in lines
  var lines = str.split('\n');

  var structureStart = false;
  var struct = "";
  for (var i = 0; i < lines.length; i++) {
    if (!structureStart && lines[i].startsWith("structure  :")){
      structureStart = true;
      struct += lines[i] + '\n';
    } else if (structureStart && lines[i].startsWith("}")){
      structureStart = false;
      struct += lines[i] + '\n';
      cons.report([{msg:struct, className: "jquery-console-message-structure"}]);
      struct = "";
    } else if (structureStart){
      struct += lines[i] + '\n';
    } else {
      cons.report([{msg:lines[i], className: "jquery-console-message"}]);
    }
  }
}

function makeSocket(code){
  // If socket is open, close it!
  if(document.socket){
    document.socket.close();
  }

  // Create new socket
  document.socket = io("",{
    path: deriveRoute()+"socket/", 
    multiplex : false, 
    upgrade: false} // Important for preventing disconection after 1 minute!
  );

  // On std-err from server
  document.socket.on('std-err',function(d){
    // Get the logs info
    var logs = JSON.parse(localStorage.logLvl);

    // Split based on the begining of the message and check if log
    if(d.startsWith("Error") && logs.e){
      cons.report([{msg:d, className: "jquery-console-message-error"}]);
    }else if(d.startsWith("Warning") && logs.w){
      cons.report([{msg:d, className: "jquery-console-message-warning"}]);
    } else if(!d.startsWith("Error") && !d.startsWith("Warning") && logs.i){
      cons.report([{msg:d, className: "jquery-console-message-info"}]);
    } 
  });

  // On start msg from server
  document.socket.on('start',function(d){
    // Clear animations and editor 
    window.animation.clearData();
    window.editor.mirror.clearGutter("core-gutter");
    coreMarkers.forEach( function(e) {
      e.clear();
    });
    $("#replSymb").click();$("#replSymb").click();

    // Activate stop button
    $('#abortButton').removeClass("inactive");

    // Start spinning instead of run button
    $('#runButton').children('img').attr("src","./images/silk/spinner.gif");

    // Get the logs info
    var logs = JSON.parse(localStorage.logLvl);
    var inLogs = [], noLogs = [];
    logs.e ? inLogs.push("Errors") : noLogs.push("Errors");
    logs.w ? inLogs.push("Warningis") : noLogs.push("Warningis");
    logs.i ? inLogs.push("Infos") : noLogs.push("Infos");

    var inMessage = inLogs.length ? "Logs will include {" + inLogs.join(", ") + "}!" : "No logs enabled!"
    var noMessage = noLogs.length ? "Logs will not include {" + noLogs.join(", ") + "}!" : "All logs enabled!"
    
    // Enable console and print info about log level
    cons.justEnable();
    cons.report([{msg: "Info:" + inMessage + " " +  noMessage, className: "jquery-console-message-info"}]);
  });

  // On stop from server
  document.socket.on('stop',function(d){
    // Stop spinning
    $('#runButton').children('img').attr("src","./images/silk/start.png");
    // Close console
    cons.disable();
    // Close socket
    document.socket.close();
    // Deactivate stop button
    $('#abortButton').addClass("inactive");
  });

  // On std-out from server
  document.socket.on('std-out',function(d){
    // Try to pars as JSON (this means it is animation)
    var out = tryToParseVisualization(d);
    if(out) {
      function setAnimationData(d) {
        window.animation.setData(d);
      }
      function handle(f, name) {
        return function(data) {
          if(data[name]) {
            f(data[name]);
            data[name] = "...";
          }
          return data;
        }
      }
      out = handle(setAnimationData, "animation")(out);
      out = handle(showCore, "core")(out);
      cons.report([{msg:JSON.stringify(out), className: "jquery-console-message"}]);
    } else {
      tryToParseStructure(d);
      // Just print on console of no animation!
      //cons.report([{msg:d, className: "jquery-console-message"}]);
    }
    // Resize; this is because animation can be added or removed!
    setSize();
  });

  // Send execute command to server
  document.socket.emit("execute", code);
}

function initCommonObjects(){
  var target = d3.select("#idpd3");
  var animation = new idpd3.Animation(target);
  animation.setIOHandler(function(json){
    var str = JSON.stringify({animation:json});
    document.socket.emit('std-in', str);
  });

  window.animation = animation;
  window.coreMarkers = [];

  $("#output").addClass("console");
  window.cons = $('#output').console({
    fadeOnReset: false,
    commandValidate:function(line){
      if (line == "") return false;
      else return true;
    },
    commandHandle:function(line,r){
      document.socket.emit("std-in",line);
      cons.report = r;
      return {}
    },
    continuedPrompt: false,
    promptHistory:true
  });
  cons.disable();
}
