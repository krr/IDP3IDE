{-# LANGUAGE OverloadedStrings #-}
module WebID.WebTypes where

import Control.Applicative
import Control.Monad
import Data.Aeson

data EvalRequest = EReq {
        idpcode :: String,
        inference :: Int,
        filename :: String,
        eInput :: String
      }
    deriving Show

instance FromJSON EvalRequest where
   parseJSON (Object v) = EReq    <$>
                          v .: "code" <*>
                          v .: "inference" <*>
                          (maybe "tmp.idp" id <$> (v .:? "filename")) <*> 
                          (maybe "" id <$> (v .:? "eInput"))

   parseJSON _          = mzero

data SimpleCode = Code String
    deriving Show

instance FromJSON SimpleCode where
  parseJSON (Object v) = Code    <$>
                         v .: "code"

  parseJSON _          = mzero

data EmptyRequest = NoRequest

instance FromJSON EmptyRequest where
   parseJSON _ = return NoRequest

data EvalResponse = ERes String String deriving Show
instance ToJSON EvalResponse where
   toJSON (ERes s s2) = object ["stdout" .= s, "stderr" .= s2]

data SaveRequest = SReq {
        scode :: String,
        spath :: String
      }
    deriving Show

instance FromJSON SaveRequest where
   parseJSON (Object v) = SReq    <$>
                          v .: "code" <*>
                          v .: "path"
   parseJSON _          = mzero

data DeleteRequest = DReq {
        dpath :: String
      }
    deriving Show

instance FromJSON DeleteRequest where
   parseJSON (Object v) = DReq    <$>
                          v .: "path"
   parseJSON _          = mzero

data GistSaveReq = GSR String String
instance ToJSON GistSaveReq where
  toJSON (GSR file vers) = object ["files" .= object 
                                      ["gistfile1.txt" .= object["content" .= file]
                                      ,"version.txt" .= object["content" .= vers]]]

maincode :: EvalRequest -> String
maincode e
      |inference e == 2 = "main()"
      |inference e == 3 = "print()"
      |inference e == 4 = ""