# IDP3-IDE

[![pipeline status](https://gitlab.com/krr/IDP3IDE/badges/v0.3.4/pipeline.svg)](https://gitlab.com/krr/IDP3IDE/-/commits/v0.3.4) 
[![Latest release](https://gitlab.com/krr/IDP3IDE/-/badges/release.svg?order_by=release_at)](https://gitlab.com/krr/IDP3IDE/-/releases/permalink/latest)

IDP3 is a knowledge-base system developed at KULeuven, you can find more on [this link](https://wms.cs.kuleuven.be/dtai/pages/software/idp/idp). 

IDP3-IDE is created as a electron modified version of the [webID](https://bitbucket.org/krr/idp-webid/commits/branch/desktop) application, which is a web-based editor for IDP3. 

## Galery

![Light theme editor](pictures/light.png){width=50%}![Dark theme editor](pictures/dark.png){width=50%}

## Installing IDP3-IDE

Check the [latest release](https://gitlab.com/krr/IDP3IDE/-/releases/permalink/latest) page and download the prebuild packageg suitable for your platform. 

## Building

### Prerequirements

- [Electron](https://www.electronjs.org/)
- [npm](https://www.npmjs.com/)
- [electron-packager](https://github.com/electron/electron-packager)
- [Haskell Stack](https://docs.haskellstack.org/en/stable/README/)

### Using building scripts

Building scripts are located in the `scripts` directory. There are separated scripts for Linux, Windows, and MacOs.
After building is successful you can find results in the `build` directory.

- **On Linux** run the build script with `./scripts/linux_build.sh` from the root of the project.
- **On MacOs** run the build script with `./scripts/mac_build.sh` from the root of the project.
- **On Windows** run the build script with `.\scripts\win_build.ps1` from the root of the project. *Note* you might have to change the PowerShell execution policy.

## Contributing

TODO

### Releaseing

Releases are automated and trigger by creation of a new **tag**. Make sure to describe changes to the application in the `release_notes.md` file, and also put them in the `CHANGELOG.md` file.


