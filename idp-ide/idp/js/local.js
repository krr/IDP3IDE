var myLayout;
var foldersToOpen;

var popovers;
var currentPopover;

const darkModePreference = window.matchMedia("(prefers-color-scheme: dark)");

// window.addEventListener('HTMLImportsLoaded', function(e) {
document.addEventListener("DOMContentLoaded", function(event) { 
    myLayout = $('#ideContainer').layout({
        closable: true, // pane can open & close
        resizable: true, // when open, pane can be resized
        slidable: true, // when closed, pane can 'slide' open over other panes - closes on mouse-out
        livePaneResizing: true,
        west__minSize: 200,
        east__minSize: 200,
        center__minWidth: 100,
        east__size: .3,
        west__size: .15,
        enableCursorHotkey: false
    });
});

// Function creating sliders
function makeslider(obj, val, min, max, step = 1, slideCallback = function(){}){
    var tooltip = $('<div id="tooltip" />').css({
        position: 'absolute',
        top: -25,
        left: -10
    }).hide();
    obj.slider({
        value: val,
        min: min,
        max: max,
        step: step,
        slide: function(event, ui) {
            tooltip.text(ui.value);
            //TODO not the best solution
            slideCallback($('#font-weight').data('slider').value());
        },
        change: function(event, ui) {
            slideCallback($('#font-weight').data('slider').value());
        }
    }).find(".ui-slider-handle").append(tooltip).hover(function() {
        tooltip.show()
    }, function() {
        tooltip.hide()
    })
}

// Checks if code in editor is up to date with the source
function isUpToDate() {
    if ($("#filename").html() === ""){
        return true;
    }

    var up = false;

    $.ajax({
      type: 'POST',
      url: "./getFile",
      data: $("#filename").html(),
      success: function(res){
        if (res === getCode()){
            up = true;
        }
      },
      async:false
    });
    return up;
}

// Loads a clicked file 
function loadPicker(e) {
    if(isUpToDate()){
        loadExample(e);
    } else {
        bootbox.confirm({
            title: '<h4><b>Unsaved file</b></h4>',
            message: '<p>Are you sure you want to change file? The curent file <code>/' + $("#filename").html() + '</code> is not saved!</p>',
            buttons: {
                confirm: {label: 'Procede', className: 'button-danger'},
                cancel: {label: 'Continue editing', className: 'button-info'}
            },
            callback: function (result) {
                if(result){
                    loadExample(e);
                }
            }
        });
    }
}

function loadExample(e) {
    $.post("./getFile", e).done(function(d) {
        $("#filename").html(e);
        window.editor.mirror.setValue(d);
        $("#fileStatus").removeClass("fa-bolt");
        $("#fileStatus").addClass("fa-check-double");
        consoleReportMessage("Success: File " + "/" + e + " opened!", "success");
    });
}

// Saves code from the editor
function save(e) {
    // Pack the file name and code
    var json = {
        "path": $("#filename").html(),
        "code": getCode()
    }

    // Save request
    $.post("./saveFile", JSON.stringify(json)).done(function(e) {
        if (e == "Success"){
            consoleReportMessage("Success: " + $("#filename").html() + " saved!", "success");
            $("#fileStatus").removeClass("fa-bolt");
            $("#fileStatus").addClass("fa-check-double");
            fileTreeInit();
        } else {
            consoleReportMessage(e);
        }
    });
}

// Function getting directories form the server
function getDir(initPath = ""){
    $.post("./listDirectories", initPath).done(function(e) {
        try {
            var e1 = e.replaceAll('\\', '/');
            data = JSON.parse(e1);
            $("#workspace-directory-input").html(data[0].path);
            $("#directories").html(data[0].list);
            $("#directories .directory-hidden").hide();
        } catch (ex) {
            consoleReportMessage(ex);
        }
    }).fail(function(e) {
        consoleReportMessage(e);
    });
}

// Open directory function (uses the rel attribute of an element)
function openDirectory(e){
    getDir($(e).prop('rel'));
}

function changeWorkspace(checkIDPafter = false){
    // Memmsage to be disolayed
    var message = "";
    var buttonLabel = "Select";

    // Check if it is first time
    if(localStorage.firstTime == 'true'){
        localStorage.firstTime = 'false';
        message += `<div class="alert alert-info" role="alert">
        You have finished the tutorial, now is the time to select a new workspace. Using the navigation below you can select the desired directory, or create a new one, that will serve as a new workspace.
        </div>`;
    }

    // Add warning if file is not saved
    if(!isUpToDate()){
        message += "<p><b>Note that the opened file is not saved, changing the workspace will result in losing these changes!</b></p>"
    }

    // Generate history if available
    var histoyArray = JSON.parse(localStorage.workSpaceHistory); 
    if(histoyArray.length){
        buttonLabel = "Change";
        message += `
        <p>Select one of the previous workspaces:</p>
        <div id="workspace-history-div" class="file-browser">
        <ul class='jqueryFileTree' style='display: block;'>`;
        for (const ws of histoyArray){
            message += '<li class="directory collapsed"><a href="#" onclick="openDirectory(this)" rel="' + ws +'"/">'+ ws +'</a></li>'
        }
        message += '</ul></div><p>Or select the new one:';
    } else {
        message += '<p>Please select the new workspace:';
    }    

    // Adding buttons for new dir and show hidden
    message += `<a title="Show hidden directories" id="controle-workspace-dir-show-hide" class="codemirror-ui-button right-button" onclick="showHiddenDirectories('directories')">
                    <img src="./images/silk/directory-show-hide.png">
                </a>
                <a title="New directory" id="controle-workspace-new-dir" class="codemirror-ui-button right-button" onclick="newFolder(true)">
                    <img src="./images/silk/newdirectory.png">
                </a></p>`;

    // Adding div to cary list of directories 
    message += '<div id="directories" class="file-browser"></div>';

    // Adding line representing actuall new path
    message += '<p>The path of new workspace is:</p><h5><code id="workspace-directory-input"></code></h5>'

    // Create a prompt
    bootbox.dialog({
        title: '<h4><b>Workspace</b></h4>',
        size: 'large',
        message: message,
        buttons: {
            cancel: {
                label: 'Cancel', 
                className: 'button-info'
            },
            confirm: {
                label: buttonLabel,
                className: 'button-success',
                callback: function() {
                    var workspacePath = $("#workspace-directory-input").html();
                    if(workspacePath !== null){
                        // Send new parh to the server            
                        var json = {
                            "path": workspacePath
                        }
                        $.post("./changeWorkspace", JSON.stringify(json)).done(function(e) {
                            // Check if workspace added sucesfully
                            if (e == "Success"){
                                consoleReportMessage("Success: Worskpace " + workspacePath + " opened!", "success");
                                // Clear the editor
                                $("#filename").html("");
                                $("#fileStatus").removeClass("fa-bolt");
                                $("#fileStatus").addClass("fa-check-double");
                                window.editor.mirror.setValue("");
                
                                // Set the path in cookie 
                                localStorage.workspacePath = workspacePath;
                                $('#current-workspace').html(localStorage.workspacePath);
                                
                                // Reset three history
                                localStorage.saveThree = "[]";
        
                                // Add the path to the history
                                var arr = JSON.parse(localStorage.workSpaceHistory);
                                arr.indexOf(workspacePath) === -1 ? arr.push(workspacePath) : null;
                                localStorage.workSpaceHistory = JSON.stringify(arr);
        
                                // Reload the file three
                                fileTreeInit();
                            } else {
                                consoleReportMessage(e);
                            }
                            
                            // If required change and check idp path after
                            if(checkIDPafter){
                                setTheIDPpath(localStorage.idpPath);
                            }
                        });
                    }
                }
            }
        },
        // Load directories when dialogue is displayed
        onShow: function(e) {
            getDir();
        }
    });
}

function showHiddenDirectories(id = "examples"){
    $("#" + id + " .directory-hidden").show();
}

function newFile(){
    if(!$('#controle-new-file').hasClass("inactive")){
        bootbox.prompt({
            title: '<h4><b>Name of the new file</b></h4>',
            message: '<p>Please enter just the name of the new file without extension. Extension <code>.idp</code> will be added automatically. New file will be added to <code>/' + localStorage.selected + '</code> directory.</p>',
            buttons: {
                confirm: {label: 'Create', className: 'button-success'},
                cancel: {label: 'Cancel', className: 'button-info'}
            },
            callback: function(name) {
                if(name != null){
                    if(localStorage.selected !== ""){
                        // Pack the file path
                        var json = {"path": localStorage.selected + name + ".idp"}
                        // Send the request
                        $.post("./createFile", JSON.stringify(json)).done(function(e) {
                            if (e == "Success"){
                                consoleReportMessage("Success: New file " + "/" + localStorage.selected + name + ".idp" + " created!", "success");
                                fileTreeInit();
                            }else {
                                consoleReportMessage(e);
                            }
                        });
                    }
                }
            }
        });
    }
}

function newFolder(workspace = false){

    // Get the path depending on situation
    var path = localStorage.selected;
    if (workspace){
        path = $("#workspace-directory-input").html();
    }

    // Create prompt
    if(workspace || !$('#controle-new-dir').hasClass("inactive")){
        bootbox.prompt({
            title: '<h4><b>Name of the new folder</b></h4>',
            message: '<p>Please enter the name of the new folder. New folder will be added to <code>/' + path + '</code> directory.</p>',
            buttons: {
                confirm: {label: 'Create', className: 'button-success'},
                cancel: {label: 'Cancel', className: 'button-info'}
            },
            callback: function(name) {
                if(name != null){
                    if(path !== ""){
                        // Send the create dir request
                        $.post("./createDir", JSON.stringify({"path": path + name + "/"})).done(function(e) {
                            // Refresh depending on situation
                            if (e == "Success"){
                                if (workspace){
                                    getDir(path);
                                } else {
                                    consoleReportMessage("Success: New directory " + "/" + path + name + "/" + " created!", "success");
                                    fileTreeInit();
                                }
                            } else {
                                consoleReportMessage(e);
                            }
                            
                        });
                    }
                }
            }
        });
    }
}

function newProject(){
    if(localStorage.workspacePath){
        bootbox.prompt({
            title: '<h4><b>Name of the new project</b></h4>',
            message: '<p>Please enter the name of the new project. New project is just a floder at the root level. Hence, new folder will be added to the <code>/</code> directory.</p>',
            buttons: {
                confirm: {label: 'Create', className: 'button-success'},
                cancel: {label: 'Cancel', className: 'button-info'}
            },
            callback: function(name) {
                if(name != null){
                    $.post("./createDir", JSON.stringify({"path": name + "/"})).done(function(e) {
                        if (e == "Success"){
                            consoleReportMessage("Success: New project " + "/" + name + "/" + " created!", "success");
                            fileTreeInit();
                        } else {
                            consoleReportMessage(e);
                        }
                    });
                }
            }
        });
    }
}

function deleteDocument() {
    if(!$('#controle-recicle').hasClass("inactive")){
        let docType = localStorage.selectedType == "true" ? 'directory <b>(together with its content)</b>' : 'file'; 
        bootbox.confirm({
            title: '<h4><b>Delete document confirmation</b></h4>',
            message: '<p>Are you sure you want to delete the selected document? If you press "Delete" the following ' + docType + ' will be removed <code>/' + localStorage.selected + '</code>.</p>',
            buttons: {
                confirm: {label: 'Delete', className: 'button-danger'},
                cancel: {label: 'Cancel', className: 'button-info'}
            },
            callback: function (result) {
                if(result){
                    if(localStorage.selected !== ""){
                        var json = {
                            "path": localStorage.selected
                        }
                        if(localStorage.selectedType == "false"){
                            $.post("./deleteFile", JSON.stringify(json)).done(function(e) {
                                if (e == "Success"){
                                    consoleReportMessage("Success: File " + "/" + localStorage.selected + " was deleted!", "success");
                                    // Clear the data in editor
                                    $("#filename").html("")
                                    $("#fileStatus").removeClass("fa-bolt");
                                    $("#fileStatus").addClass("fa-check-double");
                                    // Remove selected path
                                    localStorage.selected = ""
                                    fileTreeInit();
                                } else {
                                    consoleReportMessage(e);
                                }
                            });
                        } else {
                            $.post("./deleteDir", JSON.stringify(json)).done(function(e) {
                                if (e == "Success"){
                                    consoleReportMessage("Success: Directory " + "/" + localStorage.selected + "*" + " was deleted!", "success");
                                    // Remove folder from the list of opened folders
                                    var arr = JSON.parse(localStorage.saveThree);
                                    arr = arr.filter(el => !(el.startsWith(localStorage.selected)));
                                    localStorage.saveThree = JSON.stringify(arr);
                                    //Empty editor if file is in removed (sub)dir
                                    if($("#filename").html().startsWith(localStorage.selected) ){
                                        $("#filename").html("");
                                        $("#fileStatus").removeClass("fa-bolt");
                                        $("#fileStatus").addClass("fa-check-double");
                                    }
                                    // Remove selected path
                                    localStorage.selected = ""
                                    // Refresh the tree
                                    fileTreeInit();
                                } else {
                                    consoleReportMessage(e);
                                }
                            });
                        }
                    }
                }
            }
        });
    }
}

// Clear cookies and reload page
function resetToDefault(){
    bootbox.confirm({
        title: 'Reset confirmation',
        message: 'This action will remove all settings and reset them to default! Are you sure you want to continue?',
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'button-danger'
            },
            cancel: {
                label: 'No',
                className: 'button-info'
            }
        },
        callback: function (result) {
            if(result){
                // Clear storage
                if(localStorage){
                    localStorage.clear();
                }
                // Reload the page
                window.location.reload();
            }
        }
    });
}

// Initializes file explorer 
function fileTreeInit() {
    //Init the tree
    if(localStorage.workspacePath){
        //Enable buttons
        $('#controle-new-project').removeClass("inactive");
        $('#controle-refresh').removeClass("inactive");
        $('#examples').remove();
        $('#manual').append('<div id="examples" class="file-browser" style="min-width: 90px !important;" data-toggle="popover" data-placement="right" data-popover-content="#pop-files"></div>');
        setSize();
        $('#examples').fileTree({
            script: './listFiles',
            collapseSpeed: -1,
            expandSpeed: -1,
            root: ""
        }, function(file) {
            loadPicker(file);
            $("#picker").click();
        });

        // Expand saved directories on init
        // This must be befor the fileTree init!!! Bug in library!!! 
        $('#examples').on('filetreeinitiated', function(e, data){
            if(localStorage.showHiddenDirectories == "hide"){
                $("#examples .directory-hidden").hide();
            }
            foldersToOpen = (JSON.parse(localStorage.saveThree)).length;
            expandThree(e);
        });

        // Expand saved directories on expand
        $('#examples').on('filetreeexpanded', function (e, data){
            if(localStorage.showHiddenDirectories == "hide"){
                $("#examples .directory-hidden").hide();
            }
            if(foldersToOpen !== 0){
                foldersToOpen = foldersToOpen - 1;
                expandThree(e);
            }
        });

        // Save opened folders
        $('#examples').on('filetreeexpand', function (e, data){
            var arr = JSON.parse(localStorage.saveThree);
            arr.indexOf(data.rel) === -1 ? arr.push(data.rel) : null;
            localStorage.saveThree = JSON.stringify(arr);

            if(foldersToOpen == 0){
                reselect(data.rel, data.type);
            }
        });

        // Remove closed folders (and subfolders)
        $('#examples').on('filetreecollapsed', function (e, data){
            var arr = JSON.parse(localStorage.saveThree);
            arr = arr.filter(el => !(el.startsWith(data.rel)));
            localStorage.saveThree = JSON.stringify(arr);

            if(foldersToOpen == 0){
                reselect(data.rel, data.type);
            }
        });

        // Reselect clicked folder/file
        $('#examples').on('filetreeclicked', function (e, data){
            reselect(data.rel, data.type);
        });
    }
}

// Resizes pannels to fit nicelly vertically!
function setSize() {
    // Get the height of top bars and window
    var topHeight = $("#left-top-bar").outerHeight(true) + $("#filename").closest('.top-nav-bar-div').outerHeight(true);
    var containerHeight = $(window).height();

    // Set the editor height
    var outerH = $(".CodeMirror").outerHeight(true) - $(".CodeMirror").height();
    $("#ideContainer").height(containerHeight);
    $(".CodeMirror").height(containerHeight - topHeight - outerH);

    // Set the console height
    outerH = $("#idpd3").css('display') == 'none' ? 0 : $("#idpd3").outerHeight(true);
    outerH += $("#output").outerHeight(true) - $("#output").height();
    $("#output").height(containerHeight - topHeight - outerH);
    
    // Set the file explorer height
    outerH = $("#examples").outerHeight(true) - $("#examples").height();
    $("#rightpart").height(containerHeight);
    $("#examples").height(containerHeight - topHeight - outerH);
}

// Kills the process
function killIDP() {
    if(!$('#abortButton').hasClass("inactive")){
        document.socket.emit("abort", {});
    }
}

function clearConsole(){
    window.cons.reset();
    cons.disable();
    window.animation.clearData();
    setSize();
}

// Open colpased folders if they are in the savedThree
function expandThree(e){
    // Extrackt array
    var arr = JSON.parse(localStorage.saveThree);
    // Get the three element
    var $el = $(e.target);

    // Go over the collapsed folders 
    $el.find(".directory.collapsed").each(function (i, folder) {
        var $link = $(folder).children();
        dirrel = $link.attr('rel');
        
        // If they shuld be opened click on them
        // This is hack, but the library does not provide better solution!!!
        if(arr.indexOf(dirrel) > -1){
            // [0] is the position of the link <a> element, [1] is button! 
            $link[0].click();
        }
    }); 

    // If all folders are expanded restor selected file/folder
    if(foldersToOpen == 0){
        reselect();
    }
}

function reselect(rel = '', type){
    // Remove any selected file
    $('li.selected').removeClass('selected');

    // Set the new file in the focus
    if(rel){
        localStorage.selected = rel;
        localStorage.selectedType = type;
        $('[rel="'+rel+'"]').parent().addClass('selected');
    // Restore the selected file
    } else {
        // Select from cookie 
        if(localStorage.selected !== ""){
            if($('[rel="'+localStorage.selected+'"]')){
                $('[rel="'+localStorage.selected+'"]').parent().addClass('selected');
            } else{
                localStorage.selected = "";
            }
        }
    }

    toggleControles();
}

// Toggles CRUD controll buttons
function toggleControles(){
    if(localStorage.selected !== "" && localStorage.selectedType == "true"){
        $('#controle-recicle').removeClass("inactive");
        $('#controle-new-dir').removeClass("inactive");
        $('#controle-new-file').removeClass("inactive");
    } else if(localStorage.selected !== "" && localStorage.selectedType == "false"){
        $('#controle-recicle').removeClass("inactive");
        $('#controle-new-dir').addClass("inactive");
        $('#controle-new-file').addClass("inactive");
    } else{
        $('#controle-recicle').addClass("inactive");
        $('#controle-new-dir').addClass("inactive");
        $('#controle-new-file').addClass("inactive");
    }
}

// Sets the log level 
function toggleLogs(element, value){
    logs = JSON.parse(localStorage.logLvl);
    logs[element] = value;
    localStorage.logLvl = JSON.stringify(logs);
}

// Changes the font and update size of elements accordingly 
function changeFont(size = 14){
    // Update the storage variable
    localStorage.fontSize = size;

    // Set the font of the body
    $("body").css("font-size", localStorage.fontSize + "px");

    // Get parameters needed to update some visual ements    
    var ih = $("#faicon-id").height();
    var divh = $("#faicon-id").closest('.top-nav-bar-div').height();
    var newp = (divh - ih) / 2;

    // Update the padding of icons
    $(".glyph-status").css("padding", newp + "px");

    // Set the min height of file name (this prevents it to become small when empty) 
    $("#filename").css("min-height", divh);

    // Set the line height of the console based on new font size
    $("div.console").css("line-height", (Number(localStorage.fontSize) + 2) +"px");

    // Resize pannels
    setSize();
}

// Sets the theme 
function setTheTheme(){
    if (localStorage.themeOption == "system") {
        //const darkThemeMq = window.matchMedia("(prefers-color-scheme: dark)");
        if(darkModePreference.matches){
            localStorage.theme = "ayu-dark"
        }else{
            localStorage.theme = "elegant";
        }
    } else if (localStorage.themeOption == "dark")  {
        localStorage.theme = "ayu-dark";
    } else {
        localStorage.theme = "elegant";
    }

    // Set the css based on theme
    if (localStorage.theme == "elegant") {
        $("#themeCSS")[0].href = "./styles/light-theme.css";
        $("#idp-icon").attr("src","./images/idpico-mini.png");
    } else {
        $("#themeCSS")[0].href = "./styles/dark-theme.css";
        $("#idp-icon").attr("src","./images/idpico-mini-dark.png");
    }

    // Change the editor theme
    if (window.editor){
        window.editor.mirror.setOption("theme", localStorage.theme);
    }

}

// Shows the tutorial modal
function showTutorial(){
    // Hode help modal (when it is opened form help modal)
    $("#helpPanel").modal('hide');
    
    // Show welcome dialogue
    bootbox.dialog({
        title: 'Welcome to IDP3-IDE (' + localStorage.IDEversion + ')',
        message: `<img class="img-center" src="./images/idplogo.png" width="50%">
        <h5>Welcome to the IDP3-IDE! This program serves as an IDE for the IDP3 knowledge base system. If you are using the editor for the first time it is recommended to take a tutorial course. Otherwise, you can close this message or open directly the workspace selector.</h5>`,
        size: 'large',
        buttons: {
            cancel: {
                label: "Close",
                className: 'button-danger',
                callback: function(){
                    localStorage.firstTime = 'false';
                    setTheIDPpath(localStorage.idpPath);
                }
            },
            noclose: {
                label: "Take me to workspace selector!",
                className: 'button-info',
                callback: function() {
                    localStorage.firstTime = 'false';
                    changeWorkspace(true);
                }
            },
            ok: {
                label: "Show me tutorial!",
                className: 'button-success',
                callback: function() {
                    // List of all popovers
                    popovers = ["left-top-bar", "workspace-path-div", "examples", "buttonList", "filePickerDiv", "CodeMirror-scroll", "execute-controle-div", "help-settings-div", "output"];
                    currentPopover = 0;

                    // Show the first popover
                    $('#'+popovers[currentPopover]).popover('enable');
                    $('#'+popovers[currentPopover]).popover('show');        
                }
            }
        }
    });
}

// Close one help popoup and show the next one
function closeHelpPopup(){
    $('#'+popovers[currentPopover]).popover('hide');
    $('#'+popovers[currentPopover]).popover('disable');
    currentPopover ++;
    if(currentPopover < popovers.length){
        $('#'+popovers[currentPopover]).popover('enable');
        $("#"+popovers[currentPopover]).popover('show');
    } else if (localStorage.firstTime == 'true'){
        changeWorkspace(true);
    }
}

// Pushes an error message to the console
function consoleReportMessage(msg = "", type = "error"){
    cons.report([{msg:msg, className: "jquery-console-message-"+type}]);
    cons.disable();
}

// Sets the IDP path
function setTheIDPpath(path){
    // Try to set the idp exe path
    $.post("./changeIDPPath", JSON.stringify({"path": path})).done(function(e) {
        if (e == "Success"){
            consoleReportMessage("Success: IDP engine is set correctly!", "success");
            $('#runButton').removeClass("inactive");
        } else {
            consoleReportMessage("Error: There is an error with the IDP executable! By default, IDP3-IDE tries to use the idp3 package that comes with it. You can check this path under the settings options. If the check passes for the preselected path try to restart the IDE.");
            $('#runButton').addClass("inactive");
        }
    });
}

// Checking the new path for idp
function checkIDPPathForSettings(){
    // Clear the message field
    $("#idpPathMessage").html('<div class="alert alert-info" role="alert"><i class="fa-solid fa-gears"></i> Checking the path!</div>');

    // Get the new path.command 
    var newPath = $("#idpPath").val();

    // Small delay added as a animation
    setTimeout(() => { 
        // Check the new path/command
        $.post("./checkCustomIDPCommand", JSON.stringify({"path": newPath})).done(function(e) {
            if (e == "Success"){
                var message = "This path is already in use!"
                if(newPath !== localStorage.idpPath){
                    $("#setTheIDPPathBtn").attr("disabled", false);
                    message = "You can set this path as the new one!"
                }
                $("#idpPathMessage").html('<div class="alert alert-success" role="alert"><i class="fa-solid fa-check"></i> The selected path/command is good! ' + message + '</div>');
            } else if(e == "SpaceError"){
                $("#idpPathMessage").html('<div class="alert alert-danger" role="alert"><i class="fa-solid fa-triangle-exclamation"></i> The selected path/command contains spaces! This is not supported on Linux!</div>');
            } else {
                $("#idpPathMessage").html('<div class="alert alert-danger" role="alert"><i class="fa-solid fa-triangle-exclamation"></i> The selected path/command is not good!</div>');
            } 
        });
    }, 200);
}

// Set the IDP path from settings
function updateIDPPathFromSettings(){
    var newPath = $("#idpPath").val();
    // Update the path
    $.post("./changeIDPPath", JSON.stringify({"path": newPath})).done(function(e) {
        if (e == "Success"){
            localStorage.idpPath = newPath;
            consoleReportMessage("Success: IDP engine is set correctly!", "success");
            $("#idpPathMessage").html('<div class="alert alert-success" role="alert"><i class="fa-solid fa-thumbs-up"></i> The IDP path/command is updated!</div>');
            $('#runButton').removeClass("inactive");
        } else {
            // Normally this should newer happen as only checked paths can be set in use
            consoleReportMessage("Error: Incorrect IDP engine path/command! This path will not be set to use!");
            $("#idpPathMessage").html('<div class="alert alert-danger" role="alert"><i class="fa-solid fa-circle-exclamation"></i> The selected path is not good, hence it will not be used!</div>');
        }
    });
}

// Add listener for window close, and kill process if running
window.addEventListener('beforeunload', function (e) { 
    if(document.socket){
        document.socket.emit("abort", {});
        document.socket.close();
    } 
}); 

document.addEventListener("DOMContentLoaded", function(event) { 
    //INIT EDITOR
    $(window).resize(function() {
        setSize();
    });

    // Restore first time indicator
    if(localStorage.firstTime == undefined){
        localStorage.firstTime = 'true';
    }

    // Restore files visibility property
    if (localStorage.showHiddenDirectories == undefined){
        localStorage.showHiddenDirectories = "hide";
    } else {
        if(localStorage.showHiddenDirectories == "show"){
            $("#showHideDirs").click();
        }
    }

    // Init three memory if it is not existing
    if (localStorage.selected == undefined) {
        localStorage.selected = "";
    }

    // Init three memory if it is not existing
    if (typeof localStorage.saveThree == "undefined") {
        localStorage.saveThree = "[]";
    }

   
    // Try to restore the theme and detect system if fail
    if (localStorage.themeOption == undefined) {
        localStorage.themeOption = "system";
    } 

    // Set the radio to checked 
    $('#radio-'+localStorage.themeOption).prop('checked',true);
    setTheTheme();

    // Init logs
    if (localStorage.logLvl == undefined) {
        localStorage.logLvl = JSON.stringify({w:true, e:true, i:true});
    }

    // Restore logs checkboxes from the cookie
    logs = JSON.parse(localStorage.logLvl);        
    $("#logError").prop('checked', logs.e);
    $("#logWarning").prop('checked', logs.w);
    $("#logInfo").prop('checked', logs.i);

    // Attach even handlers to log checkboxes
    $("#logError").change(function(e){
        toggleLogs("e", $("#logError").is(":checked"));
    });
    $("#logWarning").change(function(e){
        toggleLogs("w", $("#logWarning").is(":checked"));
    });
    $("#logInfo").change(function(e){
        toggleLogs("i", $("#logInfo").is(":checked"));
    });

    // Init editor
    var te = document.getElementById("codeArea");
    var uiOptions = {
        path: 'js/',
        searchMode: 'popup'
    }
    var codeMirrorOptions = {
        mode: "idp",
        matchBrackets: true,
        lineNumbers: true,
        lineWrapping: true,
        extraKeys: {
            "Ctrl-Q": function(cm) {
                cm.foldCode(cm.getCursor());
            },
            "Ctrl-Space": "autocomplete",
            "Ctrl-Enter": function() {
                run();
            },
            "Ctrl-R": function() {
                run();
            },
            "Shift-Ctrl-D": function() {
                killIDP();
            },
            "Ctrl-S": function() {
                save();
            },
            "Esc": function(cm) {
                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            },
            "Ctrl-I": function() {
                window.editor.reindent();
            },
            "Alt-C": function() {
                $("#replSymb").click();
            }
        },
        foldGutter: true,
        gutters: ["CodeMirror-linenumbers", "core-gutter", "CodeMirror-foldgutter", "CodeMirror-lint-markers"],
        lint: true,
        theme: localStorage.theme,
        styleActiveLine: true,
        autoCloseBrackets: true
    };

    window.editor = new CodeMirrorUI(te, uiOptions, codeMirrorOptions);
    window.editor.options.lint = true;
    window.editor.mirror.state.lint.options.async = true;
    window.editor.mirror.on("change", function() {
        $("#copylink").text("");
        $("#fileStatus").removeClass("fa-check-double");
        $("#fileStatus").addClass("fa-bolt");
        localStorage.code = getCode();
        if ($("#filename").html()) {
            localStorage.filename = $("#filename").html();
        }
    });

    // Create header of editor for edit indicator and file name
    var di = $('<div>', {
        id: "filePickerDiv",
        class: "top-nav-bar-div"
    });
    di.append($('<i>', {
        class: "fa-solid glyph-status",
        id: "fileStatus"
    }));
    di.append($('<code>', {
        type: "text",
        class: "top-bar-text",
        id: "filename"
    }));
    $(di).attr("data-toggle", "popover");
    $(di).attr("data-placement", "bottom");
    $(di).attr("data-popover-content", "#pop-file-picker");
    $(di).insertAfter("#buttonList");

    // Add popover retails to editor button list
    $("#buttonList").attr("data-toggle", "popover");
    $("#buttonList").attr("data-placement", "left");
    $("#buttonList").attr("data-popover-content", "#pop-editor-controll-buttons");

    $(".CodeMirror-scroll").attr("id", "CodeMirror-scroll");
    $(".CodeMirror-scroll").attr("data-toggle", "popover");
    $(".CodeMirror-scroll").attr("data-placement", "left");
    $(".CodeMirror-scroll").attr("data-popover-content", "#pop-editor");
    
    initCommonObjects();

    // Restore replace symbols indicator
    if (localStorage.replSymb == undefined) {
        localStorage.replSymb = true;
    } else if (localStorage.replSymb == "false") {
        $("#replSymb").click();
    }
    
    // Restore the code
    $("#filename").html("");
    if (localStorage.code) {
        $("#filename").html(localStorage.filename);
        window.editor.mirror.setValue(localStorage.code);
    }

    // Init workspace history
    if (localStorage.workSpaceHistory == undefined) {
        localStorage.workSpaceHistory = "[]";
    }

    // Init workspace path
    if (localStorage.workspacePath !== undefined) {
        $('#current-workspace').html(localStorage.workspacePath);
        $.post("./changeWorkspace", JSON.stringify({"path": localStorage.workspacePath})).done(function(e) {
            // Reload the file three
            fileTreeInit();
        });
    }
    
    // Set the up to date file indicator
    if(isUpToDate()){
        $("#fileStatus").removeClass("fa-bolt");
        $("#fileStatus").addClass("fa-check-double");
    }

    // Callback for theme option
    $('input[type=radio][name=radio-theme]').change(function() {
        localStorage.themeOption = String(this.value);
        setTheTheme();
    });

    // Add event listener for the theme change
    darkModePreference.addEventListener("change", e => e && setTheTheme());

    // Callback for run button
    $("#runButton").click(function(){
        // TODO: solve this HACK running status is checked by image!!!
        if(!$('#runButton').hasClass("inactive") && $('#runButton').children('img').attr("src") == "./images/silk/start.png"){
            run();
        }
    });

    // Callback for abort/stop buttom
    $("#abortButton").click(function() {
        killIDP();
    });

    // Init symbols checkbox
    $("#symbolCSS")[0].active = true;

    // Callback for replace symbols checkbox
    $("#replSymb").click(function(){
        if($("#symbolCSS")[0].active){
            $("#symbolCSS")[0].href  = "";
            $("#symbolCSS")[0].active =false;
        }else{
            $("#symbolCSS")[0].href = "./styles/symbol.css";
            $("#symbolCSS")[0].active = true
        }
    });

    // Callback for show hidden directories
    $("#showHideDirs").click(function(){
        if(localStorage.showHiddenDirectories == "show"){
            localStorage.showHiddenDirectories = "hide";
            $("#examples .directory-hidden").hide();
        } else {
            localStorage.showHiddenDirectories = "show";
            $("#examples .directory-hidden").show();
        }
    });
    
    // Add event listener for settings modal show
    $("#configPanel").on('shown.bs.modal', function(){
        $("#idpPath").val(localStorage.idpPath);
        checkIDPPathForSettings();
    });

    // Disable path set button on input change
    // It has to be checked first 
    $('#idpPath').on('input',function(e){
        $("#setTheIDPPathBtn").attr("disabled", true);
        $("#idpPathMessage").html('<div class="alert alert-info" role="alert"><i class="fa-solid fa-list-check"></i> In order to set a new path/command, you first have to check it.</div>');
    });

    // Restore/set font size
    if (localStorage.fontSize == undefined) {
        localStorage.fontSize = 14;
    }

    // Create slides
    makeslider($("#font-weight"), localStorage.fontSize, 8, 30, 1, changeFont);

    // Set the font of the page (this will also resize the panels)
    changeFont(localStorage.fontSize);

    // Resize layout pannels
    myLayout.resizeAll();

    // Instantiate popovers
    $("[data-toggle=popover]").popover({
        html: true,
        container: 'body',
        content: function() {
        var content = $(this).attr("data-popover-content");
        return $(content).children(".popover-body").html();
        },
        title: function() {
        var title = $(this).attr("data-popover-content");
        return $(title).children(".popover-heading").html();
        }
    });

    // Disable all popovers at the begiing
    $("[data-toggle=popover]").popover('disable');

    // Get the IDE version
    $.post("./getVersion").done(function(e) {
        localStorage.IDEversion = e;
        $("#version_id").html(localStorage.IDEversion);
        if(localStorage.firstTime == 'true'){
            showTutorial();
        }
    });
    
    // Try to restore idp path
    if (localStorage.idpPath == undefined) {
        $.post("./getCurrentIDPPath").done(function(e) {
            localStorage.idpPath = e;
        });
    } else {
        setTheIDPpath(localStorage.idpPath);
    }

});
