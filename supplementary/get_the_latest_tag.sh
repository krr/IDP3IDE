#!/bin/bash

# Get the latest tag from the Git repository
latest_tag=$(git describe --tags --abbrev=0)

# Output the latest tag
echo $latest_tag