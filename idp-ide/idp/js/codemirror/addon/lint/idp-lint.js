(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
"use strict";

CodeMirror.registerHelper("lint", "idp", function(code,push,a,cm) {
      //prevents exceptions during initialising
      if(!cm)
        return "";

      var filename = $("#filename").html();

      var found = [];
      $.ajax({
        type: 'POST',
        url: "./eval",
        data: JSON.stringify({
          code: code,
          inference: 3,
          filename: filename
        }),
      }).done(function(d){
        //Error highlighting
          var re = /Error: (.*) At .*tidp:(\d+):(\d+)/g;
        var m;

        do {
            m = re.exec(d.stderr);
            if (m) {
                var tok = cm.getTokenAt(CodeMirror.Pos(m[2]-1, m[3]));
                found.push({from: CodeMirror.Pos(m[2]-1, tok.start),
                          to: CodeMirror.Pos(m[2]-1, tok.end),
                          message: m[1]
                      });
            }
        } while (m);

        //Warning highlighting
          var re2 = /Warning: (.*) At .*tidp:(\d+):(\d+)/g;
        var m = null;

        do {
            m = re2.exec(d.stderr);
            if (m) {
                var tok = cm.getTokenAt(CodeMirror.Pos(m[2]-1, m[3]));
                found.push({from: CodeMirror.Pos(m[2]-1, tok.start),
                          to: CodeMirror.Pos(m[2]-1, tok.end),
                          message: m[1],
                        severity: "warning"});
            }
        } while (m);
        push(cm,found);
      });

      return [];
    });

});
