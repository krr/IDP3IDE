echo "Building for macOS..."
# Setting up: Stack, Electron
echo "Installing stck"
brew install haskell-stack

# Building webID
echo "Building idp3-ide..."
cd idp-ide
stack build --allow-different-user
stack install --local-bin-path .
cd ..

# Build electron wrapper
cd electron-wrapper
echo "Installing electron"
npm install electron@latest
npm install --save-dev @electron/packager
echo "Building Electron wrapper..."
npx electron-packager . --platform=darwin --arch=x64 --icon=./../supplementary/idpico.ico
cd ..

# Pack files
echo "Packing files..."
mv ./electron-wrapper/idp3-ide-darwin-x64 ./idp3-ide-darwin-x64
mkdir ./idp3-ide-darwin-x64/idp3-ide.app/Contents/Resources/app/webidp/
mkdir ./idp3-ide-darwin-x64/idp3-ide.app/Contents/Resources/app/webidp/log/
cp ./idp-ide/webID ./idp3-ide-darwin-x64/idp3-ide.app/Contents/Resources/app/webidp/
cp -r ./idp-ide/idp ./idp3-ide-darwin-x64/idp3-ide.app/Contents/Resources/app/webidp/
cp ./supplementary/macos_readme.md ./idp3-ide-darwin-x64/README.md
echo "v0.3.5" > ./idp3-ide-darwin-x64/idp3-ide.app/Contents/Resources/app/webidp/version.txt

# Adding IDP3 files
echo "Adding idp3..."
wget -q https://dtai.cs.kuleuven.be/static/krr/files/releases/idp/3.7.0/idp-3.7.0-Darwin.tar.gz
mkdir ./idp3-ide-darwin-x64/idp3-ide.app/Contents/Resources/app/idp3/
tar -zxvf idp-3.7.0-Darwin.tar.gz
mv ./idp-3.7.0-Darwin/* ./idp3-ide-darwin-x64/idp3-ide.app/Contents/Resources/app/idp3/
cp ./idp3-ide-darwin-x64/ ./build/

echo "Cleaning..."
rm idp3-3.7.0-Darwin.tar.gz
rm -r idp3-3.7.0-Darwin/
