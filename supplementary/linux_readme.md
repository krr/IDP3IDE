# IDP3-IDE installation instructions (Linux)

After downloading and extracting the IDP3-IDE package, you should be reading this document.

You can use the provided app in two ways:

1) (Without launcher icon) 
    You can move the `idp3-ide-linux-x64` directory to a particular location of your choice (e.g., `~/Documents/`) and to run the IDP3-IDE you can open the directory and double click on `idp3-ide`.

2) (With launcher icon)
    1) You can move the `idp3-ide-linux-x64` directory to a particular location of your choice (recommended `/.local/share/`).
    2) Open the `idp3.desktop` file and replace `<PATHPREFIX>` with the new location of the `idp3-ide-linux-x64` directory that you selected in the previous step. Make sure to use the absolute path!
    3) Place the `idp3.desktop` in `~/.local/share/applications/`.
    4) You should be able to start IDP3-IDE from your launcher.
