#!/bin/bash

if [ $# -eq 0 ]; then

	#IDP already installed
	if [ -d "/opt/idp3/" ]; then
		read -p "It looks like IDP3 is already installed! Would you lik to delete it (ATTENTION: this will remove work directory as well, please backup your code first!!!)? [Y/n]?:" del

		if [ "$del" = "Y" ] || [ "$del" = "y" ]; then
			echo "Removing IDP3 files..."
			sudo rm /usr/share/applications/idp3.desktop
			sudo rm -r /opt/idp3/
			echo "Removing is finished! Please note that this procedure didn't remove the symlink created in the installation procedure!"
		fi
		exit
	fi

	#unzip is not installed
	if ! command -v unzip &> /dev/null
	then
		echo "unzip command could not be found, please install it and try the script again."
		echo "To install on debian based systems, try:     sudo apt install unzip"
		exit
	fi

	#wget is not installed
	if ! command -v wget &> /dev/null
	then
		echo "wget command could not be found, please install it and try the script again."
		echo "To install on debian based systems, try:     sudo apt install wget"
		exit
	fi

	# Getting files
	echo "Downloading files..."
	wget -q https://bitbucket.org/krr/idp3-desktop/downloads/idp3-desktop-app-linux-x64.zip

	# Extracting files
	echo "Extracting idp3-desktop binaries..."
	unzip -q idp3-desktop-app-linux-x64.zip

	# Replace paths in files
	prefix="/opt/idp3/"
	# IDP desktop entity
	sed -i -e "s|<PATHPREFIX>|$prefix|g" idp3.desktop
	# Electron wrapper index.js 
	sed -i -e "s|<PATHPREFIX>|$prefix|g" idp3-desktop-app-linux-x64/resources/app/index.js
	# IDP webid config
	sed -i -e "s|<PATHPREFIX>|$prefix|g" idp3-desktop-app-linux-x64/webidp/webID.cfg

	# Moving files 
	echo "Move files to opt..."
	sudo mv idp3-desktop-app-linux-x64 /opt/idp3

	echo "Creating desktop entity..."
	sudo mv idp3.desktop /usr/share/applications/

	# Clean directory
	echo "Removing temp files..."
	rm idp3-desktop-app-linux-x64.zip

	echo "Instalation finished!"
else
	echo "This script does not accept any arguments!!!"
fi
