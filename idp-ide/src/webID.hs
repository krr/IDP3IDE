{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DoAndIfThenElse #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import           Prelude                          hiding (mapM_, unlines)

import qualified Control.Concurrent.STM           as STM
import qualified Network.EngineIO.Snap            as EIOSnap
import qualified Network.SocketIO                 as SocketIO
import qualified Snap.Core                        as Snap
import qualified Snap.Http.Server                 as Snap
import qualified Snap.Util.FileServe              as Snap
import qualified Snap.Util.CORS                   as Snap
import           Control.Monad
import           Control.Monad.IO.Class           (liftIO)
import           Data.Aeson.Types
import qualified Data.Vector                      as Vector
import           Control.Applicative
import           Control.Monad.Trans.Reader       (ReaderT, ask, runReaderT)
import           Control.Monad.Trans.State.Strict (StateT)
import qualified Data.Aeson                       as Aeson

import           Control.Concurrent               (forkIO)
import qualified Data.ByteString                  as B
import           Data.Maybe                       (catMaybes)
import           Data.Text                        (Text, unlines, pack, unpack)
import           Data.Text.Encoding               (encodeUtf8)
import           Data.Text.IO                     (hGetLine)
import           Data.IORef                       
import           System.Info                      (os)
import           System.IO                        (BufferMode (NoBuffering),
                                                   hClose, hIsEOF, hPutStr,
                                                   hSetBuffering, hGetContents, Handle,
                                                   hPutStrLn, hFlush)
import           System.Process
import           Data.List                        (sort, isPrefixOf)
import           System.EasyFile                  (createDirectoryIfMissing,
                                                   doesDirectoryExist,
                                                   doesFileExist,
                                                   getDirectoryContents,
                                                   removeFile,
                                                   doesDirectoryExist,
                                                   removeDirectoryRecursive,
                                                   dropFileName,
                                                   takeFileName,
                                                   combine,
                                                   (</>),
                                                   takeExtension,
                                                   getHomeDirectory,
                                                   canonicalizePath)

import           System.Environment
import           System.Environment.Executable

import           Data.String.Conversions
import           System.Exit
import           System.IO.Temp
import           System.Process.Internals
import           System.Process (terminateProcess)
import           WebID.WebTypes
import           Control.Exception          
import           System.IO                  hiding (hGetLine)


-- Define an IORef to hold a path String
type StringRef = IORef String

-- WORKSPACE VARIABLE
-- Create a new workspace path with an initial value of ""
newStringRef :: String -> IO StringRef
newStringRef val = newIORef val 

-- Write a new value to the workspace path
writeToStringRef :: StringRef -> String -> IO ()
writeToStringRef var newValue = writeIORef var newValue

-- Read the value of the workspace path
readFromStringRef :: StringRef -> IO String
readFromStringRef var = readIORef var

-- Check if string starts wiht dot and is not ".."
-- Serves to check if file is hidden
startsWithDot :: String -> Bool
startsWithDot str = isPrefixOf "." str && str /= ".."

workingPath :: IO FilePath
workingPath = fmap fst splitExecutablePath

-- Convert Text to String
textToString :: Text -> String
textToString = Data.Text.unpack

-- Function to read the first line from a file
readFirstLine :: FilePath -> IO (Either SomeException String)
readFirstLine filePath = try $ do
    handle <- openFile filePath ReadMode
    line <- hGetLine handle
    hClose handle
    return (textToString line)

formatArgument :: String -> String 
formatArgument arg = case os of
                      "linux" -> "\'" ++ arg ++ "\'"
                      "darwin" -> "\"" ++ arg ++ "\""
                      _ -> arg

main :: IO ()
main = do
  -- Get the arguments
  args <- getArgs

  -- Create empty workspace path
  workspace <- newStringRef ""

  -- Create version string ref
  ideVersion <- newStringRef "Unknown version"

  result <- readFirstLine "version.txt"
    -- Check if the result is successful
  case result of
      Right line -> writeToStringRef ideVersion line
      Left _     -> putStrLn "Error reading version, using default value."  -- Use a default value if there's an error or the file doesn't exist


  --writeToStringRef ideVersion $ Data.Text.unpack firstLine

  -- Create IDP path string ref
  idpCommand <- newStringRef "idp"

  -- Check for the custom idp path
  case args of 
    [idpcomand] -> do
      writeToStringRef idpCommand idpcomand
    _ ->  putStrLn $ "Using the default idp command (idp)."

  -- Setup routes
  setupRoutes workspace idpCommand ideVersion

setupRoutes :: StringRef -> StringRef -> StringRef -> IO ()
setupRoutes workspace idpCommand ideVersion = do
  socketIoHandler <- SocketIO.initialize EIOSnap.snapAPI (socketServer workspace idpCommand)
  let routes =
              [
                ("idp/socket", Snap.applyCORS Snap.defaultOptions socketIoHandler)
              , ("idp/eval" , Snap.applyCORS Snap.defaultOptions $ evalLocalIDP workspace idpCommand)
              , ("idp/index.html", Snap.redirect' ("idp/") 302)
              , ("idp/", Snap.applyCORS Snap.defaultOptions $ Snap.serveDirectoryWith (Snap.defaultDirectoryConfig {Snap.indexFiles = ["local.html"]}) "./idp/")
              -- Endpoints for file explored
              , ("idp/listFiles", listFiles workspace)
              , ("idp/getFile" , getFile workspace)
              , ("idp/saveFile", saveFile workspace)
              , ("idp/deleteFile", deleteFile workspace)
              , ("idp/deleteDir", deleteDir workspace)
              , ("idp/createFile", createFile workspace)
              , ("idp/createDir", createDir workspace)
              , ("idp/changeWorkspace", changeWorkspace workspace)
              , ("idp/listDirectories", listDirectories)
              , ("idp/changeIDPPath", changeIDPPath idpCommand)
              , ("idp/getCurrentIDPPath", getCurrentIDPPath idpCommand)
              , ("idp/checkIDPCommand", checkIDPCommand idpCommand)
              , ("idp/checkCustomIDPCommand", checkCustomIDPCommand)
              , ("idp/getVersion", getVersion ideVersion)
              ]
  let serve x = Snap.httpServe (Snap.setPort 4004 Snap.defaultConfig) $ Snap.route x
  serve routes

listFiles :: StringRef -> Snap.Snap ()
listFiles workspace = do
  -- Get the workingfolder path
  base <- liftIO $ readFromStringRef workspace 
  
  path <- convertString <$> Snap.readRequestBody 10000000
  c <- liftIO $ filter (/= "..") . filter (/= ".") . sort <$> getDirectoryContents (base </> path )
  Snap.writeText "<ul class=\"jqueryFileTree\" style=\"display: none;\">"
  mapM_ (\f -> do
      isFile <- liftIO $ doesFileExist (base </> path </> f)
      if (isFile && takeExtension f == ".idp") then
        void $ Snap.writeText $ convertString
              $ "<li class=\"file ext_idp\"><a href=\"#\" rel=\""++(path</> f) ++"\">"++f++"</a></li>" else return ()
      isDir <- liftIO $ doesDirectoryExist (base </> path </> f)
      if isDir then
        if startsWithDot f then
          void $ Snap.writeText $ convertString $ "<li class=\"directory collapsed directory-hidden\"><a href=\"#\" rel=\""++(path</> f) ++"/\">"++f++"</a></li>"
        else
          void $ Snap.writeText $ convertString $ "<li class=\"directory collapsed\"><a href=\"#\" rel=\""++(path</> f) ++"/\">"++f++"</a></li>" 
      else return ()
    ) c
  Snap.writeText "</ul>"

listDirectories :: Snap.Snap ()
listDirectories = do
  -- Get the path, if empty set to home directory
  -- Additionally bring the path to canonocal form
  pathBody <- convertString <$> Snap.readRequestBody 10000000
  path <- if null pathBody 
            then do
              homeDir <- liftIO getHomeDirectory
              simplifiedPath <- liftIO $ canonicalizePath homeDir
              return simplifiedPath
            else 
              liftIO $ canonicalizePath pathBody

  exists <- liftIO $ doesDirectoryExist path
  if exists
    then do
      -- Start JSON with path and list fields
      Snap.writeText $ convertString $ "[{\"path\":\""++ path ++"/\", \"list\":\""

      c <- liftIO $ filter (/= ".") . sort <$> getDirectoryContents path
      Snap.writeText "<ul class='jqueryFileTree columnized' style='display: block;'>"
      mapM_ (\f -> do
          isDir <- liftIO $ doesDirectoryExist (path </> f)
          if isDir then
            if startsWithDot f then
              void $ Snap.writeText $ convertString $ "<li class='directory collapsed directory-hidden'><a href='#' onclick='openDirectory(this)' rel='"++ (path </> f) ++"/'>"++f++"</a></li>" 
            else
              void $ Snap.writeText $ convertString $ "<li class='directory collapsed'><a href='#' onclick='openDirectory(this)' rel='"++ (path </> f) ++"/'>"++f++"</a></li>" 
          else return ()
        ) c
      Snap.writeText "</ul>\"}]"
    else 
      -- Report an error
      Snap.writeText "Error: Selected directory does not exist!"

getFile :: StringRef -> Snap.Snap ()
getFile workspace = do
  base <- liftIO $ readFromStringRef workspace 
  b <- Snap.readRequestBody 1000000
  r <- liftIO $ readFile $ base </> convertString b
  Snap.writeText . convertString $ r

saveFile :: StringRef -> Snap.Snap ()
saveFile workspace = do
  base <- liftIO $ readFromStringRef workspace 
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  result <- liftIO $ try $ do
    value <- readFromStringRef workspace
    e <- doesFileExist (base </> spath d)
    when e $ removeFile (base </> spath d)
    createDirectoryIfMissing True $ reverse $ dropWhile (/='/') $ reverse $ base </> spath d
    writeFile (base </> spath d) (scode d)
  case result of
    Left e -> Snap.writeText "Error: " >> Snap.writeText (Data.Text.pack $ show (e :: SomeException))
    Right _ -> Snap.writeText "Success"

deleteFile :: StringRef -> Snap.Snap ()
deleteFile workspace = do
  base <- liftIO $ readFromStringRef workspace 
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  result <- liftIO $ try $ do
    e <- doesFileExist (base </> dpath d)
    when e $ removeFile (base </> dpath d)
  case result of
    Left e -> Snap.writeText "Error: " >> Snap.writeText (Data.Text.pack $ show (e :: SomeException))
    Right _ -> Snap.writeText "Success"

deleteDir :: StringRef -> Snap.Snap ()
deleteDir workspace = do
  base <- liftIO $ readFromStringRef workspace 
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  result <- liftIO $ try $ do
    e <- doesDirectoryExist (base </> dpath d)
    when e $ removeDirectoryRecursive (base </> dpath d)
  case result of
    Left e -> Snap.writeText "Error: " >> Snap.writeText (Data.Text.pack $ show (e :: SomeException))
    Right _ -> Snap.writeText "Success"

createFile :: StringRef -> Snap.Snap ()
createFile workspace = do
  base <- liftIO $ readFromStringRef workspace 
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  result <- liftIO $ try $ do
    e <- doesFileExist (base </> dpath d)
    when e $ removeFile (base </> dpath d)
    writeFile (base </> dpath d) ("")
  case result of
    Left e -> Snap.writeText "Error: " >> Snap.writeText (Data.Text.pack $ show (e :: SomeException))
    Right _ -> Snap.writeText "Success"

createDir :: StringRef -> Snap.Snap ()
createDir workspace = do
  base <- liftIO $ readFromStringRef workspace 
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  result <- liftIO $ try $ do
    e <- doesFileExist (base </> dpath d)
    when e $ removeFile (base </> dpath d)
    createDirectoryIfMissing True $ reverse $ dropWhile (/='/') $ reverse $ base </> dpath d
  case result of
    Left e -> Snap.writeText "Error: " >> Snap.writeText (Data.Text.pack $ show (e :: SomeException))
    Right _ -> Snap.writeText "Success"

changeWorkspace :: StringRef ->  Snap.Snap ()
changeWorkspace workspace = do
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  -- Check if the directory exists
  exists <- liftIO $ doesDirectoryExist (dpath d)
  if exists
    then do
      -- Change workspace 
      liftIO $ writeToStringRef workspace (dpath d);
      Snap.writeText "Success"
    else 
      -- Report an error
      Snap.writeText "Error: Selected directory does not exist!"

changeIDPPath :: StringRef ->  Snap.Snap ()
changeIDPPath idpCommand = do
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  isCommand <- liftIO $ isIDPCommandCorrect (dpath d)
  if (isCommand)
    then do 
      liftIO $ writeToStringRef idpCommand (dpath d);
      Snap.writeText "Success"
    else 
      Snap.writeText "Error"

getCurrentIDPPath :: StringRef ->  Snap.Snap ()
getCurrentIDPPath idpCommand = do
  exe <- liftIO $ readFromStringRef idpCommand 
  Snap.writeText $ convertString exe

-- Checks the current idp command
checkIDPCommand :: StringRef -> Snap.Snap ()
checkIDPCommand idpCommand = do
  exe <- liftIO $ readFromStringRef idpCommand 
  isCommand <- liftIO $ isIDPCommandCorrect exe
  if (isCommand)
    then Snap.writeText "Success"
    else Snap.writeText "Error"

checkCustomIDPCommand :: Snap.Snap ()
checkCustomIDPCommand = do
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  let path = (dpath d)  
  isCommand <- liftIO $ isIDPCommandCorrect path
 
  if ((any (== ' ') path) && os == "linux")
    then do
      Snap.writeText "SpaceError"
    else if (isCommand)
          then Snap.writeText "Success"
          else Snap.writeText "Error"

getVersion :: StringRef ->  Snap.Snap ()
getVersion ideVersion = do
  version <- liftIO $ readFromStringRef ideVersion 
  Snap.writeText $ convertString version

isIDPCommandCorrect :: String -> IO Bool
isIDPCommandCorrect exe = do
  let cmd = "\"" ++ exe ++ "\" -v"
  result <- try $ do
    (_, Just hout, _, processHandle) <- createProcess (shell cmd) { std_out = CreatePipe }
    exitCode <- waitForProcess processHandle
    case exitCode of
      ExitSuccess -> return True
      _ -> return False
  case result of
    Left (SomeException _) -> return False
    Right isWorking -> return isWorking

evalLocalIDP :: StringRef -> StringRef -> Snap.Snap ()
evalLocalIDP workspace idpCommand = do
  exe <- liftIO $ readFromStringRef idpCommand 
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  bbase <- liftIO $ readFromStringRef workspace 
  base <-fmap (</> dropFileName (filename d)) $ return $ bbase
  out <- liftIO $ withTempFile  base "tmp.tidp" (\f h -> do
    hPutStr h (idpcode d)
    hClose h
    (_,out,err) <- readProcessWithExitCodePath base exe ([takeFileName f, "-e", (formatArgument $ maincode d)]) (eInput d)
    return $ ERes out err)
  writeJSON out

readProcessWithExitCodePath
    :: String                   -- ^ Working Directory
    -> FilePath                 -- ^ Filename of the executable (see 'RawCommand' for details)
    -> [String]                 -- ^ any arguments
    -> String                   -- ^ stdinp
    -> IO (ExitCode,String,String) -- ^ exitcode, stdout, stderr
readProcessWithExitCodePath base cmd args input = do
    let exe = "\"" ++ cmd ++ "\" " ++ unwords args 
    let cp_opts = (shell exe) {
                    std_in  = CreatePipe,
                    std_out = CreatePipe,
                    std_err = CreatePipe,
                    cwd = Just base
                  }
    (Just inh ,Just outh,Just errh,ph) <- createProcess cp_opts
    hPutStr inh input
    hClose inh
    out <- hGetContents outh
    err <- hGetContents errh
    -- wait on the process
    ex <- waitForProcess ph
    return (ex, out, err)

data RunningState = RS { procH :: ProcessHandle, inph :: Handle}

socketServer :: StringRef -> StringRef -> StateT SocketIO.RoutingTable (ReaderT SocketIO.Socket Snap.Snap) ()
socketServer workspace idpCommand = do
  exe <- liftIO $ readFromStringRef idpCommand 
  curProcess <- liftIO $ STM.newTVarIO Nothing
  let killAndSwitch newVal = do {
      ph <- liftIO . STM.atomically . STM.swapTVar curProcess $ newVal;
      case ph of
        Nothing -> return ()
        Just p -> do
          liftIO $ killhandle (procH p)
          SocketIO.emit "stop"  ()
      }
  SocketIO.on "execute" $ \d -> do
        bbase <- liftIO $ readFromStringRef workspace 
        base <-fmap (</> dropFileName (filename d)) $ return $ bbase
        (pathToFile,handle) <- liftIO $ openTempFile base "tmp.tidp"
        liftIO $ do
            hPutStr handle $ idpcode d
            hClose handle
          
        let cmd = [takeFileName pathToFile, "-e", (formatArgument $ maincode d)] ++ ["-i" | inference d == 4]
        let exec = "\"" ++ exe ++ "\" " ++ unwords cmd 
        liftIO $ putStrLn exec
        let cp_opts = (shell exec) {
                std_in  = CreatePipe,
                std_out = CreatePipe,
                std_err = CreatePipe,
                cwd = Just base
              }
        (Just inh ,Just outh,Just errh,ph) <- liftIO $ createProcess cp_opts
        killAndSwitch (Just $ RS ph inh)
        SocketIO.emit "start" ()
        liftIO $ hSetBuffering outh NoBuffering
        liftIO $ hSetBuffering errh NoBuffering
        let readHandle s h = do
                 indic <- liftIO $ hIsEOF h
                 unless indic $ do
                    content <- liftIO $ unlines <$> readLineChunked h 100
                    SocketIO.emit s content
                    readHandle s h

        test <- ask :: ReaderT SocketIO.Socket IO SocketIO.Socket
        void . liftIO . forkIO $ runReaderT (readHandle "std-out" outh) test
        void . liftIO . forkIO $ runReaderT (readHandle "std-err" errh) test
        void . liftIO . forkIO $ flip runReaderT test $ do
                                            ex <- liftIO $ waitForProcess ph
                                            case ex of
                                              ExitSuccess -> killAndSwitch Nothing
                                              ExitFailure (-9) -> do
                                                    SocketIO.emit "std-err" ("Error: ExitCode (-9)" :: String)
                                                    SocketIO.emit "std-err" ("Error: The process was externally terminated (possibly by the operating system)." :: String)
                                                    return ()
                                              ExitFailure 1 -> do
                                                    SocketIO.emit "std-err" ("Error: ExitCode (1)" :: String)
                                                    SocketIO.emit "std-err" ("Error: This can be caused by parsing errors or runtime errors." :: String)
                                                    killAndSwitch Nothing
                                              ExitFailure i -> do
                                                    SocketIO.emit "std-err" ("ExitCode: "++show i)
                                                    killAndSwitch Nothing
                                            liftIO $ removeFile pathToFile

  SocketIO.on "abort" $ \NoRequest -> do 
        killAndSwitch Nothing
        return ()
  SocketIO.on "std-in" $ \s -> do
        ph <- liftIO . STM.atomically . STM.readTVar $ curProcess;
        case ph of
          Just p -> liftIO $ do
            hPutStrLn (inph p) s
            hFlush (inph p)
          Nothing -> return ()
  SocketIO.appendDisconnectHandler (killAndSwitch Nothing)


writeJSON :: ToJSON a => a -> Snap.Snap ()
writeJSON a = do
  Snap.modifyResponse $ Snap.setHeader "Content-Type" "application/json"
  Snap.writeText. convertString . Aeson.encode $ a

killhandle
    :: ProcessHandle    -- ^ A process in the process group
    -> IO ()
killhandle ph =
  terminateProcess ph
    -- withProcessHandle ph $ \case
    --   ClosedHandle _ -> return ()
    --   OpenHandle h -> do
    --     terminateProcess ph
    --     return()

contentAvailable :: Handle -> IO Bool
contentAvailable h = either (const False) id <$> (try (hReady h) :: IO (Either IOException Bool))

readLineChunked :: Handle -> Int -> IO [Text]
readLineChunked h nbOfLines =
    if nbOfLines > 0 then do
        available <- contentAvailable h
        if available then do
            c  <- hGetLine h
            cs <- readLineChunked h (nbOfLines - 1)
            return $ c:cs
        else
            return []
    else
        return []
