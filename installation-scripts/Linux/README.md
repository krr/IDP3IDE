# Usage of idp3-linux-installer script

First, you have to download the script to your local (Linux) system. 
After that open, the terminal and navigate to the place where the file is downloaded.

## Install IDP3-desktop

Execute the script with the following command: `./idp3-linux-installer.sh`. 


## Remove IDP3-desktop

To remove the IDP3-desktop app simply run `./idp3-linux-installer.sh -delete`.
Not that this will not remove the symlink created during the installation process, you will have to do this manually!

## Note

If there is a problem running the script check if execution permissions are right for the file.
You can add them by `chmod +x idp3-linux-installer.sh` and then repeat the installation command!

The script will request the superuser permissions, this is because files have to be moved to the `/opt/` directory!
